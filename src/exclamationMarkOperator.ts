const sum1 = (a?: number, b?: number): number => {
    if(a !== undefined && b !== undefined) {
        return a + b; // type narrowing
    }

    return (a || b || 0);
};

console.log('SUM1');
console.log(sum1());
console.log(sum1(1));
console.log(sum1(1, 2));

const sum2 = (a?: number, b?: number): number => {
    const c = a !== undefined && b !== undefined; // temporary variable "problem"

    if (c) {
        const numberA = a!; // looks like factorial, I know :)
        const numberB = b!;
        return numberA + numberB;
    }

    return (a || b || 0);
};

console.log('SUM2');
console.log(sum2());
console.log(sum2(1));
console.log(sum2(1, 2));