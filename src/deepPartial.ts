import _ from 'lodash';
import { DeepPartial } from 'ts-essentials';
import { some, getOrElse, mapNullable } from 'fp-ts/lib/Option'
import { pipe } from 'fp-ts/lib/pipeable';

type CompanySettings = DeepPartial<{
    isEUCompany: boolean;
    isGDPRCompliant: boolean;
    vatSettings: {
        isActiveVatPayer: boolean;
    }
}>;

const companySettings: CompanySettings = {
    vatSettings: {
        isActiveVatPayer: true,
    },
}

const isActiveVatPayer1 = (() => {
    try {
        return companySettings.vatSettings!.isActiveVatPayer! || false;
    }
    catch {
        return false;
    }
})();

const isActiveVatPayer2 = _.get(
    companySettings,
    ['vatSettings', 'isActiveVatPayer'],
    false
);

const isActiveVatPayer3 = pipe(
    some(companySettings), // a container (Option) with something meaningful in it
    mapNullable((companySettings) => companySettings.vatSettings),
    mapNullable((vatSettings) => vatSettings.isActiveVatPayer),
    getOrElse(() => false)
)

console.log(isActiveVatPayer1, isActiveVatPayer2, isActiveVatPayer3);