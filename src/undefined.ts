// noting a lack of property / value
type User = Partial<{
    citizenship: string;
}>;

const validUser1: User = {};
console.log('validUser1.citizenship: ', validUser1.citizenship);

const validUser2: User = {
    citizenship: 'PL',
};

console.log('validUser2.citizenship: ', validUser2.citizenship);

// variable value
const undefinedValue : undefined = undefined; // not of much value, but possible
let mightHoldUndefined: string | undefined = undefined;