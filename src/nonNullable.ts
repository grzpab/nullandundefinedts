const nullableNumbers = [0, 1, 2, 3, null, undefined];

const nonNullableNumbers1 = nullableNumbers.filter(Boolean)
// type definition does not work
// Boolean erases 0

// type guard (predicate)
const isNonNullable = <T>(t: T): t is NonNullable<T> => t !== null && t !== undefined;

const nonNullableNumbers2 = nullableNumbers.filter(isNonNullable);

console.log(nonNullableNumbers2);